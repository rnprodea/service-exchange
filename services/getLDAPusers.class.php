<?php
error_reporting(0);
//ini_set('log_errors',1);
//ini_set('error_log','/tmp/services.log');
define("PATH",dirname(__FILE__));
require_once(PATH.'/ArrayCollection.php');
require_once(PATH.'/cccommon.php');
// function cctools($user,$hash){
// 	touch(getcwd().'/../tokens/'.$user.'.getJID.'.$hash);
// }

// function pin($user,$hash){
// 	touch(getcwd().'/../tokens/'.$user.'.getPrivateKey.'.$hash);
// }	
//This is just a place-holder/hook for per-user web-services control
function userAccess($username,$unit,$hash,$target){
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.")\n",3,'/tmp/services.log');
	$localConfig = parse_ini_file('./services'.$unit.'_'.$target.'.ini',true);

	//Create tokens for default services available to all users with access to $unit in $target
	foreach($localConfig['services'] as $service => $value){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":tokenizing $service\n",3,'/tmp/services.log');
		touch(getcwd().'/../tokens/'.$username.'.'.$service.'.'.$hash);
	}

	//Create tokens for services available specifically for $user
	foreach($localConfig[$username] as $service => $value){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":tokenizing $service\n",3,'/tmp/services.log');
		touch(getcwd().'/../tokens/'.$username.'.'.$service.'.'.$hash);
	}
}

//This is a placeholder to ilustrate how to do per-unit access adjustments
function subinfo($username,$unit,$hash,$target){
	//touch(getcwd().'/../tokens/'.$user.'.getJID.'.$hash);
    
    // // For settings tool
	// touch(getcwd().'/../tokens/'.$user.'.getAppRegistry.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getParentalControlRatings.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getParentalControlRules.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getProvisionedRatings.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.setParentalControlPin.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.setParentalControlRatings.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.setParentalControlRules.'.$hash);

	// // For SMS Notification tool
	// touch(getcwd().'/../tokens/'.$user.'.getSmsLabelList.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getSmsMsg.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.sendSmsMsg.'.$hash);

    // // For Subscription tool
	// touch(getcwd().'/../tokens/'.$user.'.getAppRegistry.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.addSubscription.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.cancelSubscription.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getProvisionedSubscriptions.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getVodCatalogVersion.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getCurrentSubscriptions.'.$hash);
	
    // // For Order History tool
	// touch(getcwd().'/../tokens/'.$user.'.getAppRegistry.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getOrderHistoryForApp.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getVodCatalogVersion.'.$hash);
	
	// // For Device Manager tool
	// touch(getcwd().'/../tokens/'.$user.'.getUserActivationInfo2.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.removeDeviceFromUser.'.$hash);

	// // For SMS Phrase Manager tool
	// touch(getcwd().'/../tokens/'.$user.'.getSmsLabelList.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.addSmsPhrase.'.$hash);
	
	// // For PIN tool
	// touch(getcwd().'/../tokens/'.$user.'.getPrivateKey.'.$hash);
	
	// // For TechSupport tool
	// touch(getcwd().'/../tokens/'.$user.'.getMobileLogs.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getDeviceSession.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.setDeviceLogUploadEnabled.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getJidTraceEnabled.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.setJidTraceEnabled.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.getJidTraceFiles.'.$hash);
	// touch(getcwd().'/../tokens/'.$user.'.setJidOperationalStatus.'.$hash);
	
	// //For RHCInfo repors
	// touch(getcwd().'/../tokens/'.$user.'.getPagedunitRecords .'.$hash);
}
	
class getLDAPusersClass extends cccommon{

	//This is the legacy (original) mapping used by login3.php
	private $dmap = array(
		'myroslife.co.za'       => 'svg',
		'nxtonline.me'          => 'nxt',
		'healthtrial.net'       => 'spr',
		't4.roslife.net'        => 'trial4',
		't5.roslife.net'        => 'trial5',
		't6.roslife.net'        => 'trial6',
		't7.roslife.net'        => 'trial7',
		't8.roslife.net'        => 'trial8',
		'mymdom.com'            => 'mdd',
		'makooksl.net'          => 'mak',
		'roslife.in'            => 'ind',
		'wuji.in'               => 'ind',
		'narhome.az'            => 'nar',
		'totalplaysmart.com.mx' => 'top'
	);

	function getLDAPusersClass () {
		parent::__construct();
		// Define the methodTable for this class in the constructor
		$bufR = print_r($this,TRUE);
		//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::THIS = ".$bufR."\n",3,"/tmp/services.log");
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::dir = ".dirname(__FILE__)."\n",3,"/tmp/services.log");
		$this->methodTable = array(
			"getLDAPusers" => array(
				"description" => "Return true for successful authentication",
				"acccess" => "remote"
			)
		);
	}
	function encode2JSON(){
		return true;
	}
	function getLDAPusers($data) {
		$domains           = array();
		$tools             = array();
		$domains['source'] = array();
		$tools['source']   = array();
		$bufR = print_r($data,TRUE);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):DATA=".$bufR."\n",3,"/tmp/services.log");
		#$parms = array('user'=>$user,'password'=>$pass,'domain'=>$domain,'unit'=>$unit,'resource'=>$resource, 'target'=>$target);
		$bufR = print_r($parms,TRUE);
		$username = $data['user'];
		$password = $data['password'];
		$myDomain = $data['domain'];
		$unit     = $data['unit'];
		$resource = $data['resource'];
		$target   = $data['target'];
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):username=".$username."\n",3,"/tmp/services.log");
		if ( strstr($username, '\\' ) ) {
			list($domain,$user) = explode('\\',$username);
			$domain = strtolower($domain);
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):usr=".$user."\n",3,"/tmp/services.log");
		} 
		else {
			$user = $username;
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):usr=".$user."\n",3,"/tmp/services.log");
			list($domain,$uname) = explode('_',$username);
			#shell_exec("logger TOLER INFO: domain = ".$domain."\n");
			if ($domain == 'nar' ||
				$domain == 'svg' ||
				$domain == 'nxt' ||
				$domain == 'ind' ||
				$domain == 'mak' ||
				$domain == 'mdd'
				){
			#   shell_exec("logger TOLER INFO1: domain = ".$domain."\n");
			} else {
				$domain = $this->dmap[$_SERVER["SERVER_NAME"]];
				$domain = defined($domain) ? $domain : 'partner_x';
				$domain = $domain !== ""  ? $domain : 'partner_x';
			}
		}
		//$ldaprdn  = "uid=$user,ou=people,o=$domain,dc=prodeasystems,dc=net";     // ldap rdn or dn
		//$ldaprdn    = "uid=$user,ou=people,o=$domain,dc=prodeasystems,dc=net";
		//$ldaprdn    = "uid=$user,ou=people,dc=prodeasystems,dc=net";
		//$ldaprdn    = "uid=$user,ou=people,dc=prodeasystems,dc=net";
		$ldaprdn = "uid=$username, dc=prodeasystems,dc=net";
		
		$ldappass   = $password;  // associated password
		$ldapserver = $this->config['PRODEA_LDAP_IP'];
		$ldapport   = $this->config['PRODEA_LDAP_PORT'];

		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::Connecting to ldapserver = '$ldapserver', port '$ldapport', dn = '$ldaprdn\n",3,"/tmp/services.log");

		//VERIFY THAT $user's CREDENTIALS ARE VALID IN PRODEA's LDAP
		// connect to Prodea ldap server
		$ldapconn = ldap_connect($ldapserver,$ldapport) or die();
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::Connected to ldapserver = '$ldapserver', port '$ldapport', dn = '$ldaprdn\n",3,"/tmp/services.log");

		//If you were able to connect to Prodea's LDAP server then test $user's credentials
		if($ldapconn){
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::validating credentials:\n",3,"/tmp/services.log");
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::ldappass:$ldappass\n",3,"/tmp/services.log");
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::ldapserver:$ldapserver\n",3,"/tmp/services.log");
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::ldapport:$ldapport\n",3,"/tmp/services.log");
			$ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass) ;
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::credentials validated?\n",3,"/tmp/services.log");

			#$ldapbind = ldap_bind($ldapconn);
			if($ldapbind){
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::bound\n",3,"/tmp/services.log");
			}
			else{
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::NOT bound\n",3,"/tmp/services.log");
			}
		}

		//END OF $user's CREDENTIALS VERIFICATION AT PRODEA'S LDAP


		//If $user's credentials are valid in Prodea's LDAP then proceed to find out which services are enabled for it in services' LDAP
		if($ldapbind){
			//Connect to services' LDAP
			$ldapserver = $this->config['SERVICES_LDAP_IP'];
			$ldapport   = $this->config['SERVICES_LDAP_PORT'];
			$ldappass   = $this->config['SERVICES_LDAP_PASS'];
			$ldapconn   = ldap_connect($ldapserver,$ldapport) or die();

			//If you were able to connect to services' LDAP then proceed
			if ($ldapconn) {
				ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
				// binding to ldap server
				
				#$ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass) ;

				// verify binding (are user's credentials valid?)
				#if ($ldapbind) {
					//openLDAP newbie: When I log with the target user I get no tree and I can't create children.  When I log with the admin user I get the full tree.  
					//                 for that reason I am verifying the user with its credentials and then re-connect to LDAP using admin creditentials to get the tree
					//                 for $user
					$ldaprdn    = "cn=admin,dc=prodeasystems,dc=net";
					$ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass) ;
					if($ldapbind){
						//$search=ldap_search($ldapconn,$ldaprdn,'ToolAccessLevel=*');
						$ldaprdn    = "uid=$user,ou=people,dc=prodeasystems,dc=net";
						$search=ldap_search($ldapconn,$ldaprdn,'ou=*session');
						$entries = ldap_get_entries($ldapconn,$search);
						$buf = print_r($entries,true);
						error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__.")::entries:\n".$buf."\n",3,"/tmp/services.log");
					#}

				} else {
					# LDAP bind failed... 
					$error = 'LDAP binding failed:'.ldap_error($ldapconn);
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):".$error."\n",3,"/tmp/services.log");
					$ArrayOfDomains['message'] = $error;
					return( $ArrayOfDomains );
				}
			}
			else{
				$error = "Unable to connect to LDAP server";
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):".$error."\n",3,"/tmp/services.log");
				$ArrayOfDomains['message'] = $error;
				return( $ArrayOfDomains );
			}
		}
	}
}
?>
