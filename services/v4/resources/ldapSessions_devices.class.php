<?php
date_default_timezone_set('America/Chicago');
require_once('./resources/ldapSession.class.php');

class ldapSessions_devicesClass extends ldapSessionClass{
	function ldapSessions_devicesClass () {
       		parent::__construct();
	}
	function ldapSessions_devices($data){
		return parent::ldapSession($data);
	}	
}
