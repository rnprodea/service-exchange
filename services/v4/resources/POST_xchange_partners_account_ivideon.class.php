<?php
// error_reporting(-1);
// ini_set('log_errors',1);
// ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./ivideon.php');
class POST_xchange_partners_account_ivideonClass extends ivideonClass{
	function POST_xchange_partners_account_ivideonClass () {
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
        parent::__construct();
	}
	function POST_xchange_partners_account_ivideon($data){
		$target      = $data['_POST']['target'];
		$client      = $this->localConfig['client_id'];
		$token       = $this->token;

		#We need the iVideon token so we can talk to their backend
		$query = 'select ';
		$localConfig = parse_ini_file('./services_'.$target.'.ini',true);
		$URL         = $this->localConfig['accountCreationHost'].'/users?op=CREATE';
		$client      = $this->localConfig['client_id'];
		$buf = print_r($this,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":localConfig=".$buf."\n",3,'/tmp/services.log');
		//Break URI in its components
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		$error = 'none';
		$headers = array(
		    'Content-Type:multipart/form-data',
		    'Authorization:Bearer '.$token
		);
		#$URL = $localConfig['sessionURL'];
		$postDataArray = array(
			'login' => $data['login'],
		    'password' => $data['password']
		);
		$postData = '{"login":"'.$data['queryParms']['login'].'", "password":"'.$data['queryParms']['password'].'"}';
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_POST,true);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);
		curl_setopt($curl,CURLOPT_URL,$URL);
		
		#Un-comment these lines for cURL debugging
		#$curl_log = fopen("/tmp/curl.log", 'w');
		#curl_setopt($curl,CURLOPT_VERBOSE, true);
		#curl_setopt($curl,CURLOPT_STDERR,$curl_log);
		#curl_setopt($curl,CURLINFO_HEADER_OUT,true);

		#$buf = print_r($curl,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":URL=".$URL."\n",3,'/tmp/services.log');


		$results = curl_exec($curl);
		$resultsj = json_decode($results,true);
		$status  = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$info    = curl_getinfo($curl);
		$error   = curl_error($curl);
		curl_close($curl);		

		$buf = print_r($resultsj,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$buf."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$results."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":error=".$error."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":status=".$status."\n",3,'/tmp/services.log');

		if($status == 200){
			$result ='{"success":"true", "id":"'.$resultsj['result']['id'].'","login":"'.$resultsj['result']['login'].'","host":"'.$resultsj['api_host'].'"}';
		}
		else{
			$result ='{"success":"false", "code":"'.$resultsj['code'].'","message":"'.$resultsj['message'].'","error":"'.$error.'"}';
		}
		return $result;
	}
}
?>

