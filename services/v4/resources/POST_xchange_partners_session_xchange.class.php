<?php
//error_reporting(-1);
//ini_set('log_errors',1);
//ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./common.php');

class POST_xchange_partners_session_xchangeClass extends common{
	function POST_xchange_partners_session_xchangeClass () {
        parent::__construct();
	}
	function POST_xchange_partners_session_xchange($data){
		$buf = print_r($data,true);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":data:\n".$buf."\n",3,'/tmp/services.log');
		//Each $domain has its own authentication procedure.  Launch the right one based on $domain value
		#$uri = $data['_SERVER']['SCRIPT_URL'];//This is for testing only while we get the URI working
		$uri = $data['_SERVER']['REQUEST_URI'];//This is for testing only while we get the URI working
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$uri,7);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":version  = '".$version. "'\n",3,'/tmp/services.log');
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":domain   = '".$domain. "'\n",3,'/tmp/services.log');
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":project  = '".$project. "'\n",3,'/tmp/services.log');
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":resource = '".$resource. "'\n",3,'/tmp/services.log');
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":details  = '".$details. "'\n",3,'/tmp/services.log');

        $mysqluser = $GLOBALS['config']['mysql']['user'];
        $mysqlpass = $GLOBALS['config']['mysql']['pass'];
        $mysqlIP   = $GLOBALS['config']['mysql']['ip'];
        $secretkey = $GLOBALS['config']['mysql']['secretkey'];
        $time2live = $GLOBALS['config']['Basic']['inactiveTime'];

        $myuser    = $GLOBALS['queryParms']['user'];
        $mypass    = $GLOBALS['queryParms']['pass'];
        $myhost    = $GLOBALS['queryParms']['host'];

        #Connect to MySQL db
        $mysqli = new mysqli($mysqlIP, $mysqluser ,$mysqlpass, "services");
        #$query = 'select decode(`pass`,"'.$secretkey.'") as pass from `xchuser` where `name`="'.$myuser.'"';
        $query  = 'SELECT IF(STRCMP(decode(u.pass,"'.$secretkey.'"),"'.$mypass.'"),"0",';
        $query .= 'encrypt(now())) as token,u.id from `xchuser` as u join `enablers` as e on u.enabler=e.id ';
        $query .= 'where u.name="'.$myuser.'" and e.signature="'.$myhost.'"';
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":query:".$query."\n",3,'/tmp/services.log');
        $result  = $mysqli->query($query);
        $data    = $result->fetch_row();
        $token   = $data[0]==""?"0":$data[0];
        $userid  = $data[1];
        $userdata = '{}';
            
        if($token != "0"){
        
            $query = 'insert into `sessions` (`user`,`session`,`expires`) values ('.$userid.',"'.$token.'",timestampadd(second,'.$time2live.',now()))';
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.':query:'.$query."\n",3,'/tmp/services.log');
            $result = $mysqli->query($query);
            $userdata = '{"user":"'.$myuser.'","host":"'.$myhost.'","status":"valid","token":"'.$token.'"}';
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.':'.$myuser." credentials are correct\n",3,'/tmp/services.log');
        }
        else{
            $userdata = '{"user":"'.$myuser.'","host":"'.$myhost.'","status":"invalid","token":"'.$token.'"}';
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.':'.$myuser." credentials are incorrect\n",3,'/tmp/services.log');
        }
            
		//return json_encode($result);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$userdata."\n",3,'/tmp/services.log');
		return $userdata;
	}
}
?>