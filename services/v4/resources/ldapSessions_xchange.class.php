<?php
date_default_timezone_set('America/Chicago');
require_once('./resources/ldapSession.class.php');

class ldapSessions_xchangeClass extends ldapSessionClass{
	function ldapSessions_xchangeClass () {
       		parent::__construct();
	}
	function ldapSessions_xchange($data){
		return parent::ldapSession($data);
	}	
}
