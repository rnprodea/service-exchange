<?php
// error_reporting(-1);
// ini_set('log_errors',1);
// ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./ivideon.php');
class POST_xchange_partners_session_ivideonClass extends ivideon{
	function POST_xchange_partners_session_ivideonClass () {
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
        parent::__construct();
	}
	function POST_xchange_partners_session_ivideon($data){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
		$verb = $data['_SERVER']['REQUEST_METHOD'];
		$uri  = $data['_SERVER']['REQUEST_URI'];
		switch($verb ){
			case "POST":
				$result = $this->postNativeSessions($data);
				break;
			case "DELETE":
				$result = $this->deleteNativeSessions($data);
				break;
			default:
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":'$verb $uri' not supported\n",3,'/tmp/services.log');
				$result = '{"error":"'.$verb.' '.$uri.' not supported"}';
		}
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":result = '$result'\n",3,'/tmp/services.log');
		return $result;
	}

	function deleteSession($data){
		//curl to iVideon session delete goes here
	}

	function deleteNativeSessions($data){
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		list($id,$tail) = explode('/',$details,2);
		$this->deleteSession($data);
		
		//If the URI is well formed and doesn't contain extra information proceed
		if($tail == ''){
			//Load WSDL URL
			$localConfig = parse_ini_file('./services_'.$project.'.ini',true);

			if($id !== ''){
				//Remove all tokens associated to session
				foreach(glob(getcwd().'/../tokens/*.'.$id) as $token){
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Deleting token ".basename($token)."\n",3,'/tmp/services.log');
					unlink($token);
				}
				return '{"error":"none"}';
			}
			else{
				return '{"error":"Empty token value"}';
			}
		}
		//The URI contains extra information that we are not expecting
		else{
			return '{"error":"Unexpected \''.$tail.'\' information received"}';
		}
	}
	
	function postNativeSessions($data){
        // $buf = print_r($data,true);
		// error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":data:\n".$buf."\n",3,'/tmp/services.log');
		//Each $domain has its own authentication procedure.  Launch the right one based on $domain value
		#$partner  = $data['_POST']['partner'];
		$user     = $data['_SERVER']['HTTP_USER'];
		$pass     = $data['_SERVER']['HTTP_PASS'];
		$tier     = $data['_SERVER']['HTTP_TIER'];
		$domain   = $data['domain'];
		$unit     = $data['unit'];
		$target   = $data['target'];
		$hash     = md5($_SERVER['REQUEST_TIME']);
		$localConfig = parse_ini_file('./services_'.$target.'.ini',true);
		$client      = $localConfig['client_id'];
		$buf = print_r($localConfig,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":localConfig=".$buf."\n",3,'/tmp/services.log');
		//Break URI in its components
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		$error = 'none';
	}
}
?>

