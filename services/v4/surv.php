<?php
// error_reporting(-1);
// ini_set('log_errors',1);
// ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./common.php');
class survClass extends common{
        protected $localConfig = Array();
        protected $token = "0";
        function survClass(){
            parent::__construct();
            
            $path = getcwd();
            $this->localConfig = parse_ini_file('./services_surv.ini',true);
            #$bufR = print_r($this->localConfig,TRUE);
            $bufR = print_r($this,true);
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):CONFIG:\n".$bufR."\n",3,'/tmp/services.log');     
            
            #Get the db information
            $mysqluser = $this->toolsCfg['mysql']['user'];
            $mysqlpass = $this->toolsCfg['mysql']['pass'];
            $mysqlIP   = $this->toolsCfg['mysql']['ip'];
            $mysqli    = new mysqli($mysqlIP, $mysqluser ,$mysqlpass, "services");

            #Get the surv-specific information
            $clientId     = $this->localConfig['client_id'];
            $clientSecret = $this->localConfig['client_secret'];
            $sessionURL   = $this->localConfig['sessionURL'];
            
            #Get surv's user id
            $query = 'select id from `xchuser` where name="'.$clientId.'"';
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):query:".$query."\n",3,'/tmp/services.log');
            $results  = $mysqli->query($query);
            $result   = $results->fetch_row();
            $uid = $result[0];

            #Check if we have a valid surv session active
            $mytoken="0";
            $query   = 'select timestampdiff(second,now(),s.expires) as remaining,id,session from `sessions` as s where s.user='.$uid;
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):query:".$query."\n",3,'/tmp/services.log');
            $results = $mysqli->query($query);
            $count = $results->num_rows;
            if($count>0){
                while($row = $results->fetch_row()){
                    $remaining = $row[0];
                    $id        = $row[1];
                    if($remaining<1){
                        $query = 'delete from `sessions` where id='.$id;
                        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__)."(".__LINE__."):query:".$query."\n",3,'/tmp/services.log');
                        $do = $mysqli->query($query);
                        $query = 'delete from sessiondetails where sid='.$id;
                        $do = $mysqli->query($query);
                    }
                    else{
                        $mytoken = $row[2];
                    }
                }
            }
            #If $token = "0" then it means there are no valid surv tokens.  Get one
            if($mytoken == "0"){
                $headers = array(
                    'Content-Type:multipart/form-data',
                    'Authorization:Basic '. base64_encode("$clientId:$clientSecret") // <---
                );
                $URL = $this->localConfig['sessionURL'];
                $postData = array('grant_type' => 'client_credentials');
                $curl = curl_init();
                curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
                curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($curl,CURLOPT_POST,true);
                curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);
                curl_setopt($curl,CURLOPT_URL,$URL);
                
                #Un-comment these lines for cURL debugging
                #$curl_log = fopen("/tmp/curl.log", 'w');
                #curl_setopt($curl,CURLOPT_VERBOSE, true);
                #curl_setopt($curl,CURLOPT_STDERR,$curl_log);
                #curl_setopt($curl,CURLINFO_HEADER_OUT,true);
        
                #$buf = print_r($curl,true);
                error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":URL=".$URL."\n",3,'/tmp/services.log');
        
        
                $results  = curl_exec($curl);
                $resultsj = json_decode($results,true);
                $status   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $info     = curl_getinfo($curl);
                $error    = curl_error($curl);
                curl_close($curl);		
        
                $buf = print_r($resultsj,true);
                error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$buf."\n",3,'/tmp/services.log');
                error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":error=".$error."\n",3,'/tmp/services.log');
                error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":status=".$status."\n",3,'/tmp/services.log');
        
                $mytoken = $resultsj['access_token'];
                $host    = $resultsj['api_host'];
                $type    = $resultsj['token_type'];
                $expires = $resultsj['expires_in'];
                
                if($status == 200){
                    $query = 'insert into `sessions` (`user`,`session`,`expires`) values ('.$uid.',"'.$mytoken.'",timestampadd(second,'.$expires.',now()))';
                    $mysqli->query($query);
                    $sidq = 'select id from sessions where user='.$uid.' and session="'.$mytoken.'"';
                    $sid  = $mysqli->query($sidq);
                    $row  = $sid->fetch_row();
                    $sid  = $row[0];
                    $query = 'insert into sessiondetails (`sid`,`key`,`value`) values ('.$sid.',"api_host","'.$host.'"),('.$sid.',"token_type","'.$type.'")';
                    $mysqli->query($query);
                }
            }
            $this->token = $mytoken;
        }
    }
?>
