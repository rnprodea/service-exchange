<?php
//error_reporting(-1);
//ini_set('log_errors',1);
//ini_set('error_log','/tmp/services.log');
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('America/Chicago');
//function isAllowed($config,$authType,$username,$hash,$service){
function isAllowed($config,$hash,$domain,$unit,$resource){
	$service = $domain.'_'.$unit.'_'.$resource;
	//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":$service\n",3,'/tmp/services.log');
	$verb = $config['_SERVER']['REQUEST_METHOD'];
	$uri  = $config['_SERVER']['REQUEST_URI'];//['REQUEST_URI'];
	$user = $config['_SERVER']['HTTP_USER'];

	//Asume the request is going to be allowed:
	$result['allowed'] = true;
	$result['error']   = "Allowed";
	
	$sqluser  = $GLOBALS['config']['mysql']['user'];
	$sqlpass  = $GLOBALS['config']['mysql']['pass'];
	$sqlIP    = $GLOBALS['config']['mysql']['ip'];

	#Connect to MySQL db
	$mysqli = new mysqli($sqlIP, $sqluser ,$sqlpass, "services");
	
	if($resource==='session'){
		#$result['allowed'] = true;
		#$result['error'] = "Allowed";
		return ($result);
	}
	else{
		$token   = $GLOBALS['_SERVER']['HTTP_TOKEN'];
		$query   = 'select timestampdiff(second,now(),s.expires) as remaining,id from `sessions` as s where s.session="'.$token.'"';
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":query:".$query."\n",3,'/tmp/services.log');
		$active = $mysqli->query($query);
		if($active->num_rows>0){
			#$active = $active->num_rows == 0 ? 0 : $active;
			#$data    = $qresult->fetch_row();
			#$active = $data[0];
			$bufR = print_r($active,true);
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":active:".$bufR."\n",3,'/tmp/services.log');
			$remaining = $active->fetch_row();
			if($remaining[0] < 1){
				$query = 'delete from sessions where session="'.$token.'"';
				$do = $mysqli->query($query);
				$query = 'delete from sessiondetails where sid='.$remaining[1];
				$do = $mysqli->query($query);
				$result['allowed'] = false;
				$result['error']   = "Denied";
			}
		}
		else{
			$result['allowed'] = false;
			$result['error'] = "Not Found";
		}
		return $result;
	}
}
//First: Load the configuration file
$config = parse_ini_file('./services.ini',true);
foreach($config as $key => $value){
	$GLOBALS['config'][$key] = $value;
}

// If the request is coming from the new HTML5/JavaScript version then process it as a regular PHP otherwise handle it as a AMFPHP request
//$uri     = $_SERVER['HTTP_URI'];//['REQUEST_URI'];
$uri     = $_SERVER['REQUEST_URI'];
$hash    = $_SERVER['HTTP_HASH'];
$user    = $_SERVER['HTTP_USER'];

#Capture 'target' value if passed as a header
#$GLOBALS['queryParms']['target'] = $_SERVER['HTTP_TARGET'];
//The format of the URL is:
//https://sccservices.prodeasystems.net:8443/services/v2/toolsPortal/subinfo/session?target=druv
//Where:
//	https://sccservices.prodeasystems.net:8443/ = $null
//	services/                                   = $void
//	v2/                                         = $version
//	toolsPortal/                                = $domain
//	subinfo/                                    = $unit
//	session?target=druv                             = $resource
list($null,$void,$version,$domain,$unit,$resource,$details) = explode('/',$uri,7);
//If $uri is not malformed then proceed to extrat POST data
if(!defined($details)){
	//If we have post-data form then retrieve it
	$tail = "";
	foreach($_POST as $key => $value){
		$GLOBALS['queryParms'][$key] = $value;
		$tail .= $key.'='.$value.'&';
	}

	foreach($_GET as $key => $value){
		$GLOBALS['queryParms'][$key] = $value;
		$tail .= $key.'='.$value.'&';
	}	
	//Remove the last '&' 
	$tail = substr($tail, 0, -1);

	//If we have query post-data then retrieve it
	list($resource,$query) = explode('?',$resource,2);
	$qp = explode('&',$query);
	foreach($qp as $parm){
		if($parm!=""){
			list($key,$value) = explode('=',$parm);
			$GLOBALS['queryParms'][$key]=$value;
		}
	}

	$GLOBALS['queryParms']['domain']   = $domain;
	$GLOBALS['queryParms']['unit']   = $unit;
	$GLOBALS['queryParms']['resource'] = $resource;
	$GLOBALS['queryParms']['details']  = $details;

	//Update $_SERVER['REQUEST_URI'] to reflect the POST-form information
	$_SERVER['REQUEST_URI'] .= $query=="" ? '?'.$tail :'&'.$tail;
}
else{
	//$uri is malformed
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__."Malformed URI\n",3,'/tmp/services.log');
	$result = '{"token":"unknown","error":"Malformed URI"}';
	echo $result;
	return;
}
$buf = print_r($GLOBALS,true);
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":GLOBALS:\n".$buf."\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":version      = '".$version."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":domain       = '".$domain."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":unit       = '".$unit."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":details      = '".$details."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":resource     = '".$resource."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":query        = '".$query."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceFile  = '".$serviceFile."'\n",3,'/tmp/services.log');

	$target  = $GLOBALS['queryParms']['target'];
	$target  = $target == '' ? 'xchange':$target;
	$verb    = $GLOBALS['_SERVER']['REQUEST_METHOD'];
	#$service = $verb.'_'.$domain.'_'.$unit.'_'.$resource.'_'.$target;
	$service = $verb.'_'.$domain.'_'.$unit.'_'.$resource;
	
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":user:'".$user."'\n",3,'/tmp/services.log');
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":domain:'".$domain."'\n",3,'/tmp/services.log');
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":unit:'".$unit."'\n",3,'/tmp/services.log');
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":target:'".$target."'\n",3,'/tmp/services.log');
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":hash:'".$hash."'\n",3,'/tmp/services.log');
	$hash    = md5($user.$domain.$unit.$target.$hash);
#}

error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":hash:'".$hash."'\n",3,'/tmp/services.log');
$request = isAllowed($GLOBALS,$hash,$domain,$unit,$resource);
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":uri:'".$uri."',resource='".$resource."',user='".$user."',details='".$details."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":final service = '".$service."'\n",3,'/tmp/services.log');

if($request['allowed']){
	//Load the target PHP class and instantiate it
	//$serviceFile = getcwd().'/'.$version.'/'.$service.".class.php";
	$serviceFile = getcwd().'/resources/'.$service.".class.php";
	$serviceClass = $service.'Class';
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceFile  = '".$serviceFile."'\n",3,'/tmp/services.log');
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceClass = '".$serviceClass."'\n",3,'/tmp/services.log');
	try{
		include_once($serviceFile);
		$myService = new $serviceClass;
	}
	catch (Exception $e){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":Unable to include ".$serviceFile."\n",3,'/tmp/services.log');
	}

	// Make sure we can serve the request with the loaded PHP class
	if(method_exists($myService,$service)){
		$result = $myService->$service($GLOBALS);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$result."\n",3,'/tmp/services.log');
	}
	else{
		$error = 'function '.$service.'() not found in '.$serviceFile;
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".$error."\n",3,'/tmp/services.log');
		//$result = json_encode('{"error":"'.$request['error'].'"}');
		$result = '{"token":"unknown","error":"'.$request['error'].'"}';
	}
}
else{
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".$request['error']."\n",3,'/tmp/services.log');
	//$result = json_encode('{"error":"'.$request['error'].'"}');
	$result = '{"token":"unknown","error":"'.$request['error'].'"}';
}
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$result."\n",3,'/tmp/services.log');
echo   $result;
//return $result;
?>
