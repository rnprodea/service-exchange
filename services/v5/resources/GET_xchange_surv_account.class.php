<?php
// error_reporting(-1);
// ini_set('log_errors',1);
// ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./surv.php');
class GET_xchange_surv_accountClass extends survClass{
	function GET_xchange_surv_accountClass () {
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
        parent::__construct();
	}
	function GET_xchange_surv_account($data){
        $token       = $this->token;
        
        #Connect to MySQL db in order to get the host where the request needs to be sent for this particular session
        $mysqluser = $this->toolsCfg['mysql']['user'];
        $mysqlpass = $this->toolsCfg['mysql']['pass'];
        $mysqlIP   = $this->toolsCfg['mysql']['ip'];
        $secretkey = $this->toolsCfg['mysql']['secretkey'];

        $session   = $GLOBALS['_SERVER']['HTTP_TOKEN'];

        $myuser    = $GLOBALS['queryParms']['user'];
        $mypass    = $GLOBALS['queryParms']['pass'];
        $myhost    = $GLOBALS['queryParms']['host'];

        $mysqli = new mysqli($mysqlIP, $mysqluser ,$mysqlpass, "services");
        $query  = 'select id from sessions where session="'.$token.'"';
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":query=".$query."\n",3,'/tmp/services.log');
        $sidr   = $mysqli->query($query);
        $sida   = $sidr->fetch_row();
        $sid    = $sida[0];
        $query  = 'select `value` from sessiondetails where `key`="api_host" and `sid`='.$sid;
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":query=".$query."\n",3,'/tmp/services.log');
        $hostr  = $mysqli->query($query);
        $hosta  = $hostr->fetch_row();
        $host   = $hosta[0];
        $URL    = "";

		$buf = print_r($this,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":localConfig=".$buf."\n",3,'/tmp/services.log');
		//Break URI in its components
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
        $error = 'none';
        
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_HTTPHEADER,$this->headers);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_POST,true);
        $result = "{}";
        $id = $this->queryParms['id'];
        if($this->queryParms['login']){
            $URL      = 'https://'.$host.'/users?op=GET_ID';#'{"login":"'.$data['queryParms']['login'].'"}';
            curl_setopt($curl,CURLOPT_URL,$URL);
            $postData = '{"login":"'.$this->queryParms['login'].'"}';
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":postdata=".$postdata."\n",3,'/tmp/services.log');
            // $postdata = array(
            //     'login' => $this->queryParms['login']
            // );
            curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":URL=".$URL."\n",3,'/tmp/services.log');
            $results  = curl_exec($curl);
            $resultsj = json_decode($results,true);
            $status   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $info     = curl_getinfo($curl);
            $error    = curl_error($curl);
            #curl_close($curl);		

            $buf = print_r($resultsj,true);
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$buf."\n",3,'/tmp/services.log');
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$results."\n",3,'/tmp/services.log');
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":error=".$error."\n",3,'/tmp/services.log');
            error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":status=".$status."\n",3,'/tmp/services.log');

            if($status == 200){
                $id         = $resultsj['result']['id'];
            }
            else{
                $message = "Unknown reason";
                switch ($status) {
                    case 400:
                        $message =  "Wrong API Host";
                        break;
                }
                $result ='{"success":"false","id":"unknown","login":"'.$this->queryParms['login'].'","deleted":"true","last_login":"unknown"}';
                error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":result=".$result."\n",3,'/tmp/services.log');
                return($result);
            }
        }
        $URL    = 'https://'.$host.'/users/'.$id.'?op=GET';#'{"login":"'.$data['queryParms']['login'].'"}';
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":URL=".$URL."\n",3,'/tmp/services.log');
        curl_setopt($curl,CURLOPT_URL,$URL);
        $results = curl_exec($curl);
        $resultsj = json_decode($results,true);
        $status  = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $info    = curl_getinfo($curl);
        $error   = curl_error($curl);
        curl_close($curl);		

        $buf = print_r($resultsj,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$buf."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$results."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":error=".$error."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":status=".$status."\n",3,'/tmp/services.log');

        if($status == 200){
            $id         = $resultsj['result']['id'];
            $login      = $resultsj['result']['login'];
            $deleted    = $resultsj['result']['deleted'] ? "true": "false";
            $last_login = $resultsj['result']['last_login'];
            $result ='{"success":"true", "id":"'.$id.'","login":"'.$login.'","deleted":"'.$deleted.'","last_login":"'.$last_login.'"}';

        }
        else{
            $message = "Unknown reason";
            switch ($status) {
                case 400:
                    $message =  "Wrong API Host";
                    break;
            }
            $result ='{"success":"false", "status":"'.$status.'", "message":"'.$message.'"}';
        }
		return $result;
	}
}
?>

