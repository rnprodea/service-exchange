<?php
//error_reporting(-1);
//ini_set('log_errors',1);
//ini_set('error_log','/tmp/services.log');
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('America/Chicago');

function isAllowed($verb,$user,$inactiveTime,$hash,$domain,$unit,$resource){
	$service = $domain.'_'.$unit.'_'.$resource;

	//First of all... do housekeeping....
	$noTokens  = true;//Assume that there is no sessions (active or inactive) at all
	$loggedOut = true;//Assume that there is no $hash session for $user
	$expired   = false;//Assume that,if available, $hash session hasnt expired yet

	//Asume the request is going to be allowed:
	$result['allowed'] = true;
	$result['error']   = "Allowed";
	
	//Clean all sessions that have been inactive for more than $config['Basic']['inactiveTime'] seconds
	foreach(glob(getcwd().'/../tokens/'.$user.'.loggedSince.*') as $lastTimeFile){
		$noTokens  = false;                                //We found, at least, 1 session lingering
		$readAttempts = 1;
		//We might have multiple back-to-back web service calls trying to read $lastTime
		//lets pace them down so everybody can read the file
		while($readAttempts < 11){
			//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": ".$user.'.'.$lastHash." attempts:".$readAttempts."\n",3,'/tmp/services.log');
			$lastTime  = file_get_contents($lastTimeFile);
			
			//If we couldnt read $lastTimeFile then wait 10ms and try again otherwise continue
			if($lastTime === FALSE){
				$readAttempts++;
				usleep(10000);
			}
			else{
				$readAttempts=11;
			}
		}
		
		//Now get the hash associated with $lastTimeFile
		$pieces    = explode('/',$lastTimeFile);
		$pieces    = explode('.',$pieces[sizeof($pieces)-1]);
		$lastHash  = $pieces[2];
		$loggedOut = $hash === $lastHash?false:$loggedOut; //If we find $hash to be still 'active' ($hash==$lastHash) then $loggedOut flag is false
		//If $config['Basic']['inactiveTime'] seconds have past since the last time $lastHash $user made a request then force a log back in
		$timeElapsed = $lastTime !== FALSE?$_SERVER['REQUEST_TIME']-$lastTime:-1;
		//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": ".$user.'.'.$lastHash." session is ".$timeElapsed." secs old\n",3,'/tmp/services.log');
		//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":timeOut value=".$config['Basic']['inactiveTime']."\n",3,'/tmp/services.log');
		//if($timeElapsed>$config['config']['Basic']['inactiveTime']){
		if($timeElapsed>$inactiveTime){
			$denyCurrent = false;
			foreach(glob(getcwd().'/../tokens/*'.$lastHash) as $token){
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Deleting token ".basename($token)."\n",3,'/tmp/services.log');
				unlink($token);
				
				//If this inactive $lastHash happens to be associated to $user then deny the request
				if($hash === $lastHash){
					$denyCurrent = true;
				}
			}
			if($denyCurrent){	
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Denying $verb $resource request for $user.$hash session\n",3,'/tmp/services.log');
				$result['allowed'] = false;
				$result['error']   = "Logged out for inactivity";
				$expired           = true;
			}
		}	
	}

	//Now that we are done with housekeeping, proceed to validate if $user with $hash id can execute $service
	//If we are trying to log in then allow the attempt
	if($expired){
		return $result;
	}
	else{
		if($noTokens||$loggedOut){
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Denying $verb $resource request for $user.$hash  session\n",3,'/tmp/services.log');;
			$result['allowed'] = false;
			$result['error']   = "Not logged in";
			return $result;
		}
		else{
			//If couldnt get the last time $user request a service with $lastHash ($timeElapsed==-1) then declare the request as timed out
			if($timeElapsed == -1){
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Denying $verb $resource request for $user.$hash  session\n",3,'/tmp/services.log');;
				$result['allowed'] = false;
				$result['error']   = "Request timed out";
				return $result;
			}
			else{	
				//Check if there is a token for $service
				$verb = $_SERVER['REQUEST_METHOD'];
				if(file_exists(getcwd().'/../tokens/'.$user.'.'.$verb.'_'.$service.'.'.$hash)){
					//$user is allowed to execute $service.  Reset time reference to current time
					//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Allowing ".$service." request for ".$user.".".$hash." session\n",3,'/tmp/services.log');
					file_put_contents(getcwd().'/../tokens/'.$user.'.loggedSince.'.$hash,$_SERVER['REQUEST_TIME']);
					return ($result);
				}
				else{
					//$user is not allowed to execute $service under $hash session.  Return indicating that $service is not supported
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Denying $verb $resource request for $user.$hash  session\n",3,'/tmp/services.log');;
					$result['allowed'] = false;
					$result['error']   = "Request not supported";
					return $result;
				}
			}
		}
	}
}

function gethost($verb,$domain,$unit,$resource){

	return $host;
}
// ============================================================
//
//              M A I N   S E R V E R   C O D E 
// 
// ============================================================

//First: Load the configuration file
$config = parse_ini_file('./services.ini',true);
foreach($config as $key => $value){
	$GLOBALS['config'][$key] = $value;
}

//Collect the basic information
$uri     = $_SERVER['REQUEST_URI'];
$hash    = $_SERVER['HTTP_HASH'];
$user    = $_SERVER['HTTP_USER'];

//Break URI into its components for further processing
list($null,$void,$version,$domain,$unit,$resource,$target,$junk) = explode('/',$uri,7);
//If URI is malformed then just abort
if(defined $junk){
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__."Malformed URI\n",3,'/tmp/services.log');
	$result = '{error":"Malformed URI"}';
	echo $result;
	return;
}

//Initialize critical variables
//$tail = "";

//Collect POST data if available
foreach($_POST as $key => $value){
	$GLOBALS['queryParms'][$key] = $value;
	//$tail .= $key.'='.$value.'&';
}

//Collect GET data if availavble
foreach($_GET as $key => $value){
	$GLOBALS['queryParms'][$key] = $value;
	//$tail .= $key.'='.$value.'&';
}	

//If $target is defined then check if it contains query parameters, if it is not defined then check if 
// $resource has query parameters
//$requestQuery = defined $target ? $target : $resource;
if(defined $target){
	//If we have query post-data then retrieve it
	list($target,$query) = explode('?',$target,2);
	$qp = explode('&',$query);
	foreach($qp as $parm){
		if($parm!=""){
			list($key,$value) = explode('=',$parm);
			$GLOBALS['queryParms'][$key]=$value;
			//$tail .= $key.'='.$value.'&';
		}
	}
}
else{
	//If we have query post-data then retrieve it
	list($resource,$query) = explode('?',$resource,2);
	$qp = explode('&',$query);
	foreach($qp as $parm){
		if($parm!=""){
			list($key,$value) = explode('=',$parm);
			$GLOBALS['queryParms'][$key]=$value;
			//$tail .= $key.'='.$value.'&';
		}
	}
}
// //Remove the last '&' 
// $tail = substr($tail, 0, -1);





$verb         = $GLOBALS['_SERVER']['REQUEST_METHOD'];
$host         = getHost($verb,$domain,$unit,$resource);
$service      = $verb,'_'.$resource;
$path         = $domain.'/'.$unit.'/'.$host.'/'.$service;
$hash         = md5($user.$domain.$unit.$host.$verb.$resource.$hash);
$inactiveTime = $GLOBALS['config']['Basic']['inactiveTime'];

//Set critical GLOBAL values
$GLOBALS['queryParms']['verb']     = $verb;
$GLOBALS['queryParms']['domain']   = $domain;
$GLOBALS['queryParms']['unit']     = $unit;
$GLOBALS['queryParms']['host']     = $host;
$GLOBALS['queryParms']['resource'] = $resource;

$buf = print_r($GLOBALS,true);
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":GLOBALS :\n".$buf."\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":uri     :'".$uri."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":user    :'".$user."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":hash    :'".$hash."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":verb    :'".$verb."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":domain  :'".$domain."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":unit    :'".$unit."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":resource:'".$resource."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":host    :'".$host."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":target  :'".$target."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":path    :'".$path."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":sercvice:'".$service."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":version :'".$version."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":details :'".$details."'\n",3,'/tmp/services.log');

//Verify if $user's $hash session is allowed to execute $verb_$resource web service call on $target at $domain/$unit/$host
$request = isAllowed($verb,$user,$inactiveTime,$hash,$domain,$unit,$resource);


if($request['allowed']){
	//Load the target PHP class and instantiate it
	//$serviceFile = getcwd().'/'.$version.'/'.$service.".class.php";
	$serviceFile = getcwd().'/resources/'.$service.".class.php";
	$serviceClass = $service.'Class';
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceFile  = '".$serviceFile."'\n",3,'/tmp/services.log');
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceClass = '".$serviceClass."'\n",3,'/tmp/services.log');
	try{
		include_once($serviceFile);
		$myService = new $serviceClass;
	}
	catch (Exception $e){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":Unable to include ".$serviceFile."\n",3,'/tmp/services.log');
	}

	// Make sure we can serve the request with the loaded PHP class
	if(method_exists($myService,$service)){
		$result = $myService->$service($GLOBALS);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$result."\n",3,'/tmp/services.log');
	}
	else{
		$error = 'function '.$service.'() not found in '.$serviceFile;
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".$error."\n",3,'/tmp/services.log');
		//$result = json_encode('{"error":"'.$request['error'].'"}');
		$result = '{"token":"unknown","error":"'.$request['error'].'"}';
	}
}
else{
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".$request['error']."\n",3,'/tmp/services.log');
	//$result = json_encode('{"error":"'.$request['error'].'"}');
	$result = '{"token":"unknown","error":"'.$request['error'].'"}';
}
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$result."\n",3,'/tmp/services.log');
echo   $result;
//return $result;
?>
