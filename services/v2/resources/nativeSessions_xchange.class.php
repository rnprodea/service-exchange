<?php
date_default_timezone_set('America/Chicago');
require_once('./common.php');
class nativeSessions_xchangeClass extends common{
	function nativeSessions_xchangeClass ($data) {
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
        parent::__construct();
	}
	function nativeSessions_xchange($data){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
		$verb = $data['_SERVER']['REQUEST_METHOD'];
		$uri  = $data['_SERVER']['REQUEST_URI'];
		switch($verb ){
			case "POST":
				$result = $this->postNativeSessions($data);
				break;
			case "DELETE":
				$result = $this->deleteNativeSessions($data);
				break;
			default:
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":'$verb $uri' not supported\n",3,'/tmp/services.log');
				$result = '{"error":"'.$verb.' '.$uri.' not supported"}';
		}
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":result = '$result'\n",3,'/tmp/services.log');
		return $result;
	}

	function deleteSession($data){
		//curl to iVideon session delete goes here
	}

	function deleteNativeSessions($data){
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		list($id,$tail) = explode('/',$details,2);
		$this->deleteSession($data);
		
		//If the URI is well formed and doesn't contain extra information proceed
		if($tail == ''){
			//Load WSDL URL
			$localConfig = parse_ini_file('./services_'.$project.'.ini',true);

			if($id !== ''){
				//Remove all tokens associated to session
				foreach(glob(getcwd().'/../tokens/*.'.$id) as $token){
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Deleting token ".basename($token)."\n",3,'/tmp/services.log');
					unlink($token);
				}
				return '{"error":"none"}';
			}
			else{
				return '{"error":"Empty token value"}';
			}
		}
		//The URI contains extra information that we are not expecting
		else{
			return '{"error":"Unexpected \''.$tail.'\' information received"}';
		}
	}
	
	function postNativeSessions($data){
        $buf = print_r($data,true);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":data:\n".$buf."\n",3,'/tmp/services.log');
		//Each $domain has its own authentication procedure.  Launch the right one based on $domain value
		$partner  = $data['_POST']['partner'];
		$user     = $data['_SERVER']['HTTP_USER'];
		$pass     = $data['_SERVER']['HTTP_PASS'];
		$tier     = $data['_SERVER']['HTTP_TIER'];
		$localConfig = parse_ini_file('./services_'.$partner.'.ini',true);
		$client      = $localConfig['client_id'];
		$buf = print_r($localConfig,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":localConfig=".$buf."\n",3,'/tmp/services.log');
		//Break URI in its components
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		$error = 'none';
		$headers = array(
		    'Content-Type:multipart/form-data',
		    'Authorization:Basic '. base64_encode("$user:$pass") // <---
		);
		$URL = $localConfig['sessionURL'];
		$postData = array('grant_type' => 'client_credentials');
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_POST,true);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);
		curl_setopt($curl,CURLOPT_URL,$URL);
		
		#Un-comment these lines for cURL debugging
		#$curl_log = fopen("/tmp/curl.log", 'w');
		#curl_setopt($curl,CURLOPT_VERBOSE, true);
		#curl_setopt($curl,CURLOPT_STDERR,$curl_log);
		#curl_setopt($curl,CURLINFO_HEADER_OUT,true);

		#$buf = print_r($curl,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":URL=".$URL."\n",3,'/tmp/services.log');


		$results = curl_exec($curl);
		$resultsj = json_decode($results,true);
		$status  = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$info    = curl_getinfo($curl);
		$error   = curl_error($curl);
		curl_close($curl);		

		$buf = print_r($resultsj,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$buf."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":error=".$error."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":status=".$status."\n",3,'/tmp/services.log');

        $token = $resultsj['access_token'];
        $host  = $resultsj['api_host'];
        $type  = $resultsj['token_type'];
		// //Create a SOAP client
		// $client = new SoapClient($wsdl,array('exceptions'=>0));
		
		// //Log into xchange using $client
		// $token = $client->login($user,$pass);
		// if(is_soap_fault($token)){
		// 	$error = $token->faultstring;
		// 	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":error = ".$error."\n",3,'/tmp/services.log');
		// 	$result = '{"token":"unknown","error":"'.$error.'"}';
		// }
		if($status == 200){
			$result = '{"token":"'.$token.'","host":"'.$host.'","type":"'.$type.'","status":"'.$status.'","error":"none"}';
			//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":token = ".$token."\n",3,'/tmp/services.log');
			//Create the time reference for future requests
			file_put_contents(getcwd().'/../tokens/'.$user.'.loggedSince.'.$token,$_SERVER['REQUEST_TIME']);
			
			//Now create tokens for all services available to $user
			foreach($localConfig['services'] as $service => $value){
				//This is a rudimentary implementation of tiering support
				if($tier >= $value){
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":tokenizing $service\n",3,'/tmp/services.log');
					touch(getcwd().'/../tokens/'.$user.'.'.$service.'.'.$token);
				}
			}
			#$result = '{"token":"'.$token.'","error":"none"}';
		}
		else{
			$result = '{"token":"unknown","host":"unknown","type":"unknown","status":"'.$status.'","error":"'.$error.'"}';
		}
		return $result;
	}
}
?>

