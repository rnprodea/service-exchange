<?php
date_default_timezone_set('America/Chicago');
require_once('./resources/ldapSession.class.php');

class ldapSessions_toolsPortalClass extends ldapSessionClass{
	function ldapSessions_toolsPortalClass() {
        parent::__construct();
	}
	function ldapSessions_toolsPortal($data){
		return parent::ldapSession($data);
	}
}
