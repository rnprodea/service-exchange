<?php
//error_reporting(-1);
//ini_set('log_errors',1);
//ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./common.php');

class serviceClass extends common{
	function serviceClass () {
        parent::__construct();
	}
	function service($data){
		//Each $domain has its own authentication procedure.  Launch the right one based on $domain value
		$uri  = $data['_SERVER']['REQUEST_URI'];//This is for testing only while we get the URI working
		$verb = $data['_SERVER']['REQUEST_METHOD'];
		$hash = $data['hash'];
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$uri,7);
		list($resource,$query) = explode('?',$resource);
		$service      = $verb.'_'.$resource;

		//If $user is allowed to execute $service then proceed else return error
		$user = $data['user'];
		if(file_exists(getcwd().'/../tokens/'.$user.'.'.$service.'.'.$hash)){
			//$serviceFile  = getcwd().'/'.$version.'/'.$service.".class.php";
			$serviceFile  = getcwd().'/resources/'.$service.".class.php";
			$serviceClass = $service.'Class';
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceFile  = '".$serviceFile."'\n",3,'/tmp/services.log');
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceClass = '".$serviceClass."'\n",3,'/tmp/services.log');
			try{
				include_once($serviceFile);
				$myService = new $serviceClass($GLOBALS);
			}
			catch (Exception $e){
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".$serviceFile." not found\n",3,'/tmp/services.log');
			}
			// Make sure we can serve the request with the loaded PHP class
			if(method_exists($myService,$service)){
				//set user's tier to 3 so all services are available to it.  This is just a placeholder for possible tiering in the future. The tiering should
				//be defined/queried internally (not passed in the REQUEST message) in order to effectively enforce tiering
				$GLOBALS['_SERVER']['HTTP_TIER']=3; //If in the future TIER value is included in the request then this line needs to be removed
				$result = $myService->$service($GLOBALS);
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$result."\n",3,'/tmp/services.log');
			}
			else{
				//$result = json_encode('{"error":"'.$service.' not supported"}');
				$error = 'function '.$service.'() not found in '.$serviceFile;
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":$error\n",3,'/tmp/services.log');
				$result = '{"error":"requested service is not supported"}';
			}		
			
			//return json_encode($result);
			//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$result."\n",3,'/tmp/services.log');
			//return $result;
			return $result;
		}
		else{
			$result = '{"error":"'.$user.'/'.$service.'/'.$hash.' combination is denied access to the service}"';
			return $result;
		}
	}
}
?>
