<?php
//error_reporting(-1);
//ini_set('log_errors',1);
//ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./common.php');
class GET_xmppstatusClass extends common{
	function GET_xmppstatusClass($data) {
        parent::__construct();
	}

	function GET_xmppstatus($data){
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		list($resource,$query) = explode('?',$resource);
		$buf = print_r($data,true);
		//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":data = ".$buf."\n",3,'/tmp/services.log');

		//Get the real value of customer domain based off of $dp
		require_once(dirname(__FILE__).'/../../domainmap.class.php');
		$dmc    = new domainmapClass();
		$domain = $dmc->domainmap($data['queryParms']['dp']);
		$data['customerDomain'] = $domain;

		//Now get the jid we are looking for
		$jid = $data['queryParms']['jid'];

		//Connect to SCC's xcp_db database
		$dbh  = new PDO("pgsql:dbname=xcp_db;host=dal2-dbs", 'readonly',"");

		//Define the query to obtain the information needed
		$result = '{"jid":"'.$jid."@".$domain.'","in":"Not found","out":"Not found","status":"unknown"}';
		$query = "select jid,login_stamp,logout_stamp  from users where jid='".$jid."@".$domain."'";
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":query = ".$query."\n",3,'/tmp/services.log');
		foreach ($dbh->query($query) as $row) {
			$buf   = print_r($row,true);
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":row = ".$buf."\n",3,'/tmp/services.log');
			$jid    = $row['jid'];
			$in     = $row['login_stamp'];
			$out    = $row['logout_stamp'];
			$inDate = date_create($in);
			$outDate= date_create($out);
			$xmpp   = $inDate>$outDate ? 'Connected' : 'unknown';
			$xmpp   = $outDate>$inDate ? 'Disconnected' : $xmpp;

			$result = '{"jid":"'.$jid.'","in":"'.$in.'","out":"'.$out.'","status":"'.$xmpp.'"}';
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result = ".$result."\n",3,'/tmp/services.log');
		}
		return $result;
	}
}
?>
