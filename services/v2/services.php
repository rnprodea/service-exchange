<?php
//error_reporting(-1);
//ini_set('log_errors',1);
//ini_set('error_log','/tmp/services.log');
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('America/Chicago');
//function isAllowed($config,$authType,$username,$hash,$service){
function isAllowed($config,$hash,$service){
	//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":$service\n",3,'/tmp/services.log');
	$verb = $config['_SERVER']['REQUEST_METHOD'];
	$uri  = $config['_SERVER']['REQUEST_URI'];//['REQUEST_URI'];
	$user = $config['_SERVER']['HTTP_USER'];
	//First of all... do housekeeping....
	$noTokens  = true;//Assume that there is no sessions (active or inactive) at all
	$loggedOut = true;//Assume that there is no $hash session for $user
	$expired   = false;//Assume that,if available, $hash session hasnt expired yet

	//Asume the request is going to be allowed:
	$result['allowed'] = true;
	$result['error']   = "Allowed";
	
	//Clean all sessions that have been inactive for more than $config['Basic']['inactiveTime'] seconds
	foreach(glob(getcwd().'/../tokens/'.$user.'.loggedSince.*') as $lastTimeFile){
		$noTokens  = false;                                //We found, at least, 1 session lingering
		$readAttempts = 1;
		//We might have multiple back-to-back web service calls trying to read $lastTime
		//lets pace them down so everybody can read the file
		while($readAttempts < 11){
			//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": ".$user.'.'.$lastHash." attempts:".$readAttempts."\n",3,'/tmp/services.log');
			$lastTime  = file_get_contents($lastTimeFile);
			
			//If we couldnt read $lastTimeFile then wait 10ms and try again otherwise continue
			if($lastTime === FALSE){
				$readAttempts++;
				usleep(10000);
			}
			else{
				$readAttempts=11;
			}
		}
		
		//Now get the hash associated with $lastTimeFile
		$pieces    = explode('/',$lastTimeFile);
		$pieces    = explode('.',$pieces[sizeof($pieces)-1]);
		$lastHash  = $pieces[2];
		$loggedOut = $hash === $lastHash?false:$loggedOut; //If we find $hash to be still 'active' ($hash==$lastHash) then $loggedOut flag is false
		//If $config['Basic']['inactiveTime'] seconds have past since the last time $lastHash $user made a request then force a log back in
		$timeElapsed = $lastTime !== FALSE?$_SERVER['REQUEST_TIME']-$lastTime:-1;
		//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": ".$user.'.'.$lastHash." session is ".$timeElapsed." secs old\n",3,'/tmp/services.log');
		//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":timeOut value=".$config['Basic']['inactiveTime']."\n",3,'/tmp/services.log');
		if($timeElapsed>$config['config']['Basic']['inactiveTime']){
			$denyCurrent = false;
			foreach(glob(getcwd().'/../tokens/*'.$lastHash) as $token){
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Deleting token ".basename($token)."\n",3,'/tmp/services.log');
				unlink($token);
				
				//If this inactive $lastHash happens to be associated to $user then deny the request
				if($hash === $lastHash){
					$denyCurrent = true;
				}
			}
			if($denyCurrent){	
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Denying $verb $uri request for $user.$hash session\n",3,'/tmp/services.log');
				$result['allowed'] = false;
				$expired           = true;
				$result['error']   = "Logged out for inactivity";
			}
		}	
	}

	//Now that we are done with housekeeping, proceed to validate if $user with $hash id can execute $service
	//If we are trying to log in then allow the attempt
	//if($service==='getAuthToken'){
	if($service==='session'){
		$result['allowed'] = true;
		$result['error'] = "Allowed";
		return ($result);
	}
	else{
		if($expired){
			return $result;
		}
		else{
			if($noTokens||$loggedOut){
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Denying $verb $uri request for $user.$hash  session\n",3,'/tmp/services.log');;
				$result['allowed'] = false;
				$result['error']   = "Not logged in";
				return $result;
			}
			else{
				//If couldnt get the last time $user request a service with $lastHash ($timeElapsed==-1) then declare the request as timed out
				if($timeElapsed == -1){
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Denying $verb $uri request for $user.$hash  session\n",3,'/tmp/services.log');;
					$result['allowed'] = false;
					$result['error']   = "Request timed out";
					return $result;
				}
				else{	
					//Check if there is a token for $service
					$verb = $_SERVER['REQUEST_METHOD'];
					if(file_exists(getcwd().'/../tokens/'.$user.'.'.$verb.'_'.$service.'.'.$hash)){
						//$user is allowed to execute $service.  Reset time reference to current time
						//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Allowing ".$service." request for ".$user.".".$hash." session\n",3,'/tmp/services.log');
						file_put_contents(getcwd().'/../tokens/'.$user.'.loggedSince.'.$hash,$_SERVER['REQUEST_TIME']);
						return ($result);
					}
					else{
						//$user is not allowed to execute $service under $hash session.  Return indicating that $service is not supported
						error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Denying $verb $uri request for $user.$hash  session\n",3,'/tmp/services.log');;
						$result['allowed'] = false;
						$result['error']   = "Request not supported";
						return $result;
					}
				}
			}
		}
	}
}
//First: Load the configuration file
$config = parse_ini_file('./services.ini',true);
foreach($config as $key => $value){
	$GLOBALS['config'][$key] = $value;
}
//Define the supported authentication methods
$authMethod['Native'] = 'nativeSessions';
$authMethod['LDAP']   = 'ldapSessions';
$authMethod['Proxy']  = 'proxySessions';

// If the request is coming from the new HTML5/JavaScript version then process it as a regular PHP otherwise handle it as a AMFPHP request
//$uri     = $_SERVER['HTTP_URI'];//['REQUEST_URI'];
$uri     = $_SERVER['REQUEST_URI'];
$hash    = $_SERVER['HTTP_HASH'];
$user    = $_SERVER['HTTP_USER'];
//The format of the URL is:
//https://sccservices.prodeasystems.net:8443/services/v2/toolsPortal/subinfo/session?dp=druv
//Where:
//	https://sccservices.prodeasystems.net:8443/ = $null
//	services/                                   = $void
//	v2/                                         = $version
//	toolsPortal/                                = $domain
//	subinfo/                                    = $target
//	session?dp=druv                             = $resource
list($null,$void,$version,$domain,$target,$resource,$details) = explode('/',$uri,7);
//If $uri is not malformed then proceed to extrat POST data
if(!defined($details)){
	//If we have post-data form then retrieve it
	$tail = "";
	foreach($_POST as $key => $value){
		$GLOBALS['queryParms'][$key] = $value;
		$tail .= $key.'='.$value.'&';
	}
	//Remove the last '&' 
	$tail = substr($tail, 0, -1);

	//If we have query post-data then retrieve it
	list($resource,$query) = explode('?',$resource,2);
	$qp = explode('&',$query);
	foreach($qp as $parm){
		if($parm!=""){
			list($key,$value) = explode('=',$parm);
			$GLOBALS['queryParms'][$key]=$value;
		}
	}

	//Update $_SERVER['REQUEST_URI'] to reflect the POST-form information
	$_SERVER['REQUEST_URI'] .= $query=="" ? '?'.$tail :'&'.$tail;
}
else{
	//$uri is malformed
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__."Malformed URI\n",3,'/tmp/services.log');
	$result = '{"token":"unknown","error":"Malformed URI"}';
	echo $result;
	return;
}
$buf = print_r($GLOBALS,true);
//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":GLOBALS:\n".$buf."\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":version      = '".$version."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":domain       = '".$domain."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":target       = '".$target."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":details      = '".$details."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":resource     = '".$resource."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":query        = '".$query."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceFile  = '".$serviceFile."'\n",3,'/tmp/services.log');

$authType= $_SERVER['HTTP_ATYPE'];//'Native', 'LDAP' or 'Proxy'
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":hash    :'".$hash."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":authType:'".$authType."'\n",3,'/tmp/services.log');
if($resource == 'session'){
	$service = $authMethod[$authType];
}
else{
	$service = $resource;
}
if($authType=='LDAP'){
	//$nid = md5($username.$project.$dp.$hash);//md5($username.$report.$dp.$data['hash']);
	//$hash= md5($username.$report.$dp.$data['hash']);
	$dp = $GLOBALS['queryParms']['dp'];
	$hash = md5($user.$target.$dp.$hash);
}

error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":hash:'".$hash."'\n",3,'/tmp/services.log');
$request = isAllowed($GLOBALS,$hash,$resource);
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":uri:'".$uri."',resource='".$resource."',user='".$user."',details='".$details."'\n",3,'/tmp/services.log');
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":final service = '".$service."'\n",3,'/tmp/services.log');

if($request['allowed']){
	//Load the target PHP class and instantiate it
	//$serviceFile = getcwd().'/'.$version.'/'.$service.".class.php";
	$serviceFile = getcwd().'/resources/'.$service.".class.php";
	$serviceClass = $service.'Class';
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceFile  = '".$serviceFile."'\n",3,'/tmp/services.log');
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":serviceClass = '".$serviceClass."'\n",3,'/tmp/services.log');
	try{
		include_once($serviceFile);
		$myService = new $serviceClass;
	}
	catch (Exception $e){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":Unable to include ".$serviceFile."\n",3,'/tmp/services.log');
	}

	// Make sure we can serve the request with the loaded PHP class
	if(method_exists($myService,$service)){
		$result = $myService->$service($GLOBALS);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$result."\n",3,'/tmp/services.log');
	}
	else{
		$error = 'function '.$service.'() not found in '.$serviceFile;
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".$error."\n",3,'/tmp/services.log');
		//$result = json_encode('{"error":"'.$request['error'].'"}');
		$result = '{"token":"unknown","error":"'.$request['error'].'"}';
	}
}
else{
	error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".$request['error']."\n",3,'/tmp/services.log');
	//$result = json_encode('{"error":"'.$request['error'].'"}');
	$result = '{"token":"unknown","error":"'.$request['error'].'"}';
}
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":result:".$result."\n",3,'/tmp/services.log');
echo   $result;
//return $result;
?>
