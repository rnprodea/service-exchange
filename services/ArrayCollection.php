<?php

class ArrayCollection {
	public $_explicitType = "flex.messaging.io.ArrayCollection";
	var $source = array();

	function ArrayCollection( $array ) {
		if ( !isset($array) ) {
			$array = array();
		}
		$this->source = $array;
	}
}

?>
