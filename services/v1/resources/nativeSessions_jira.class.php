<?php
date_default_timezone_set('America/Chicago');
require_once('./common.php');
class nativeSessions_jiraClass extends common{
	function nativeSessions_jiraClass ($data) {
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
        parent::__construct();
	}
	function nativeSessions_jira($data){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
		$verb = $data['_SERVER']['REQUEST_METHOD'];
		$uri  = $data['_SERVER']['REQUEST_URI'];
		switch($verb ){
			case "POST":
				$result = $this->postNativeSessions($data);
				break;
			case "DELETE":
				$result = $this->deleteNativeSessions($data);
				break;
			default:
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":'$verb $uri' not supported\n",3,'/tmp/services.log');
				$result = '{"error":"'.$verb.' '.$uri.' not supported"}';
		}
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":result = '$result'\n",3,'/tmp/services.log');
		return $result;
	}
	function deleteNativeSessions($data){
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		list($id,$tail) = explode('/',$details,2);
		
		//If the URI is well formed and doesn't contain extra information proceed
		if($tail == ''){
			//Load WSDL URL
			$localConfig = parse_ini_file('./services_'.$project.'.ini',true);
			$wsdl     = $localConfig['wsdl'];

			//Create a SOAP client
			$client = new SoapClient($wsdl,array('exceptions'=>0));

			//Log out from  JIRA using $client
			$result = $client->logout($id);		

			if($id !== ''){
				//Remove all tokens associated to session
				foreach(glob(getcwd().'/../tokens/*.'.$id) as $token){
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Deleting token ".basename($token)."\n",3,'/tmp/services.log');
					unlink($token);
				}
				return '{"error":"none"}';
			}
			else{
				return '{"error":"Empty token value"}';
			}
		}
		//The URI contains extra information that we are not expecting
		else{
			return '{"error":"Unexpected \''.$tail.'\' information received"}';
		}
	}
	
	function postNativeSessions($data){
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
		//Break URI in its components
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		$error = 'none';
		//curl -k "https://tools.prodeasystems.net:8443/sr/jira.issueviews:searchrequest-printable/temp/SearchRequest.html?type=47&type=53&type=48&type=46&type=43&pid=10231&customfield_10466=druv01264&tempMax=1000&os_username=ops_rnagles&os_password=GAN_765mar"

		//Each $domain has its own authentication procedure.  Launch the right one based on $domain value
		$localConfig = parse_ini_file('./services_'.$project.'.ini',true);
		$wsdl     = $localConfig['wsdl'];
		$user     = $data['_SERVER']['HTTP_USER'];
		$pass     = $data['_SERVER']['HTTP_PASS'];
		$tier     = $data['_SERVER']['HTTP_TIER'];
error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":wsdl=".$wsdl."\n",3,'/tmp/services.log');
		//Create a SOAP client
		$client = new SoapClient($wsdl,array('exceptions'=>0));
		
		//Log into JIRA using $client
		$token = $client->login($user,$pass);
		if(is_soap_fault($token)){
			$error = $token->faultstring;
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":error = ".$error."\n",3,'/tmp/services.log');
			$result = '{"token":"unknown","error":"'.$error.'"}';
		}
		else{
			//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":token = ".$token."\n",3,'/tmp/services.log');
			//Create the time reference for future requests
			file_put_contents(getcwd().'/../tokens/'.$user.'.loggedSince.'.$token,$_SERVER['REQUEST_TIME']);
			
			//Now create tokens for all services available to $user
			foreach($localConfig['services'] as $service => $value){
				//This is a rudimentary implementation of tiering support
				if($tier >= $value){
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":tokenizing $service\n",3,'/tmp/services.log');
					touch(getcwd().'/../tokens/'.$user.'.'.$service.'.'.$token);
				}
			}
			$result = '{"token":"'.$token.'","error":"none"}';
		}
		return $result;
	}
}
?>

