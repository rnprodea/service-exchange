<?php
date_default_timezone_set('America/Chicago');
//error_reporting(-1);
//ini_set('display_errors',1);
require_once('./common.php');
class GET_ticketsClass extends common{
	function GET_ticketsClass($data) {
        parent::__construct();
	}

	function GET_tickets($data){
		list($null,$void,$version,$domain,$project,$resource,$jid,$count,$junk) = explode('/',$data['_SERVER']['REQUEST_URI'],9);
		$user = $data['_SERVER']['HTTP_USER'];
		$pass = $data['_SERVER']['HTTP_PASS'];
		$localConfig = parse_ini_file('./services_'.$project.'.ini',true);
		$baseURL     = $localConfig['lastNURI'];
		$URL         = $baseURL.$jid.'&os_username='.$user.'&os_password='.$pass.'&tempMax='.$count;
		
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":URL = ".$URL."\n",3,'/tmp/services.log');
		$error = '{"error":"Malformed or unsupported URI"}';
		if(!isset($junk)){
			$curl = curl_init();
			curl_setopt($curl,CURLOPT_USERAGENT,DEFAULT_USERAGENT);
			curl_setopt($curl,CURLOPT_HEADER,$headers);
			curl_setopt($curl,CURLOPT_HTTPGET,true);
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,DEFAULT_TIMEOUT);
			curl_setopt($curl,CURLOPT_TIMEOUT, DEFAULT_TIMEOUT);
			curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl,CURLOPT_URL,$URL);
			$results = curl_exec($curl);
			$status  = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			
			//JIRA3.x's HTML, as it is currently returned, can't be loaded by PHP's $curl->loadXML() so we have to 'clean' it 
			//before passing it onto PHP's CURL library
			$startTag = '<table id="issuetable"';
			$endTag   = '</table>';
			$sIdx = strpos($results,$startTag);
			$eIdx = strpos($results,$endTag,$sIdx)+strlen($endTag);
			$size = $eIdx - $sIdx;
			$tableXML = substr($results,$sIdx,$size);
			
			//PHP's DOMDocument can't handle empty 'nowrap' attribute... we don't need it so just delete it
			$tableXML = str_replace('nowrap','',$tableXML);

			//<img> tags are coming 'opened' so we need to either remove or close them
			$pattern = '/<img.*><\/a>/';
			$tableXML = preg_replace($pattern,'</a>',$tableXML);
			
			// This is another way of getting the XML if PHP is not compiled with cURL:
			// $cmd = 'curl -H \'X-Atlassian-Token: no-check\' -X GET -k "'.$URL.'"';
			// error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":results:\n".$tableXML."\n",3,'/tmp/services.log');
			// $test = exec($cmd,$resultsArray,$status);
		
			//By default create an empty result
			$tickets='{}';
			
			//If we could cURL $URL then process it
			if($status ==  200){
				$table = new DOMDocument();
				$table->loadXML($tableXML);
				
				//Initialize $tickets
				$tickets = '{"tickets":[';
				
				//Extract all 'tr' tags from $table
				$trs = $table->getElementsByTagName('tr');
				
				//For each 'tr' tag, extract 'td' tags if it is not the banner tr
				foreach($trs as $tr){
					if($tr->attributes->getNamedItem('id') !== null){
						$tds = $tr->getElementsByTagName('td');
						
						//Extract the information needed from each 'td' tag
						foreach($tds as $td){
							$class  = $td->attributes->getNamedItem('class');
							$cValue = $class->nodeValue;
							$a      = $td->getElementsByTagName('a');
							$aValue = $a->length>0?$a->item(0)->nodeValue:'';
							$aHref  = $a->length>0?$a->item(0)->attributes->getNamedItem('href')->nodeValue:'';
							if($class !== null & $cValue === 'nav issuekey'){
								$key  = trim($td->nodeValue);
								$href = trim($aHref); 
							}
							if($class !== null & $cValue === 'nav summary'){
								$summary = trim($td->nodeValue);
							}
						}
						$tickets .= '{"id":"'.$key.'","summary":"'.$summary.'","self":"'.$href.'"},';
					}
				}
				$tickets = rtrim($tickets,',');
				$tickets .= ']}';
			}
			return $tickets;	
		}
		else{
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__FUNCTION__."(".__LINE__."):'$junk' data is not supported\n",3,'/tmp/toolsportal.log');
			return $error;	
		}
		
	}
}
?>
