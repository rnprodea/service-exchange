<?php
// error_reporting(-1);
// ini_set('log_errors',1);
// ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./common.php');
class POST_xchange_partners_account_ivideonClass extends common{
	function POST_xchange_partners_account_ivideonClass () {
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
        parent::__construct();
	}
	function POST_xchange_partners_account_ivideon($data){
        // $buf = print_r($data,true);
		// error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":data:\n".$buf."\n",3,'/tmp/services.log');
		// //Each $domain has its own authentication procedure.  Launch the right one based on $domain value
		$partner  = $data['_POST']['partner'];
		$user     = $data['_SERVER']['HTTP_USER'];
		$hash     = $data['_SERVER']['HTTP_HASH'];
		$tier     = $data['_SERVER']['HTTP_TIER'];
		$domain   = $data['domain'];
		$unit     = $data['unit'];
		$target   = $data['target'];
		$token    = $data['queryParms']['token'];
		$localConfig = parse_ini_file('./services_'.$target.'.ini',true);
		$URL         = $localConfig['accountCreationHost'].'/users?op=CREATE';
		$client      = $localConfig['client_id'];
		$buf = print_r($localConfig,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":localConfig=".$buf."\n",3,'/tmp/services.log');
		//Break URI in its components
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		$error = 'none';
		$headers = array(
		    'Content-Type:multipart/form-data',
		    'Authorization:Bearer '.$token
		);
		#$URL = $localConfig['sessionURL'];
		$postDataArray = array(
			'login' => $data['login'],
		    'password' => $data['password']
		);
		$postData = '{"login":"'.$data['queryParms']['login'].'", "password":"'.$data['queryParms']['password'].'"}';
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_POST,true);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);
		curl_setopt($curl,CURLOPT_URL,$URL);
		
		#Un-comment these lines for cURL debugging
		#$curl_log = fopen("/tmp/curl.log", 'w');
		#curl_setopt($curl,CURLOPT_VERBOSE, true);
		#curl_setopt($curl,CURLOPT_STDERR,$curl_log);
		#curl_setopt($curl,CURLINFO_HEADER_OUT,true);

		#$buf = print_r($curl,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":URL=".$URL."\n",3,'/tmp/services.log');


		$results = curl_exec($curl);
		$resultsj = json_decode($results,true);
		$status  = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$info    = curl_getinfo($curl);
		$error   = curl_error($curl);
		curl_close($curl);		

		$buf = print_r($resultsj,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$buf."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$results."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":error=".$error."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":status=".$status."\n",3,'/tmp/services.log');

		if($status == 200){
			//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":token = ".$token."\n",3,'/tmp/services.log');
			//Create the time reference for future requests
			$hash = md5($user.$domain.$unit.$target.$hash);
			file_put_contents(getcwd().'/../tokens/'.$user.'.loggedSince.'.$hash,$_SERVER['REQUEST_TIME']);
			
			//Now create tokens for all services available to $user
			foreach($localConfig['services'] as $service => $value){
				//This is a rudimentary implementation of tiering support
				#if($tier >= $value){
				list($verb,$resource) = explode('_',$service,2);
				$service = $verb.'_'.$domain.'_'.$target.'_'.$resource;
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":tokenizing $service\n",3,'/tmp/services.log');
					touch(getcwd().'/../tokens/'.$user.'.'.$service.'.'.$hash);
				#}
			}
			$result ='{"success":"true", "id":"'.$resultsj['result']['id'].'","login":"'.$resultsj['result']['login'].'","host":"'.$resultsj['api_host'].'"}';

		}
		else{
			$result ='{"success":"false", "code":"'.$resultsj['code'].'","message":"'.$resultsj['message'].'","error":"'.$error.'"}';
		}
		return $result;
	}
}
?>

