<?php
date_default_timezone_set('America/Chicago');
//error_reporting(-1);
//ini_set('log_errors',1);
//ini_set('error_log','/tmp/services.log');
require_once('./common.php');
class ldapSessionClass extends common{
	function ldapSessionClass() {
        parent::__construct();
	}
	function ldapSession($data){
		$verb = $data['_SERVER']['REQUEST_METHOD'];
		$uri  = $data['_SERVER']['REQUEST_URI'];
		switch($verb ){
			case "POST":
				$result = $this->postLdapSession($data);
				break;
			case "DELETE":
				$result = $this->deleteLdapSession($data);
				break;
			default:
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":'$verb $uri' not supported\n",3,'/tmp/services.log');
				$result = '{"error":"'.$verb.' '.$uri.' not supported"}';
		}
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":result = '$result'\n",3,'/tmp/services.log');
		return $result;
	}
	function deleteLdapSession($data){
		//$buf = print_r($data,true);
		//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":data:\n".$buf."\n",3,'/tmp/services.log');
		list($null,$void,$version,$domain,$unit,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		if(!defined($details)){
			list($resource,$tail) = explode('?',$resource,2);
			list($p1,$p2,$junk)   = explode('&',$tail,3);
			list($key,$value)     = explode('=',$p1,2);${$key} = $value;
			list($key,$value)     = explode('=',$p2,2);${$key} = $value;
			
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":version :".$version."\n",3,'/tmp/services.log');
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":domain  :".$domain."\n",3,'/tmp/services.log');
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":unit :".$unit."\n",3,'/tmp/services.log');
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":resource:".$resource."\n",3,'/tmp/services.log');
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":target      :".$target."\n",3,'/tmp/services.log');
			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":hash    :".$hash."\n",3,'/tmp/services.log');
			
			//If the URI is well formed and doesn't contain extra information proceed
			if(!defined($junk)){
				//Remove all tokens associated to session
				$username = $data['_SERVER']['HTTP_USER'];
				$nid = md5($username.$unit.$target.$hash);//md5($username.$report.$target.$data['hash']);
				$count=0;
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":hash    :".$nid."\n",3,'/tmp/services.log');
				foreach(glob(getcwd().'/../tokens/*'.$nid) as $token){
					error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.": Deleting token ".basename($token)."\n",3,'/tmp/services.log');
					unlink($token);
					$count++;
				}
				$error = $count>0 ?"none":"No tokens found";
				return '{"error":"'.$error.'"}';
			}
			//The URI contains extra information that we are not expecting
			else{
				return '{"error":"Unexpected \''.$tail.'\' information received"}';
			}
		}
		else{
			return '{"error":"Unexpected \''.$details.'\' information received"}';
		}
	}
	
	function postLdapSession($data){
		//Break URI in its components
		list($null,$void,$version,$domain,$unit,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":version :".$version."\n",3,'/tmp/services.log');
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":domain  :".$domain."\n",3,'/tmp/services.log');
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":unit :".$unit."\n",3,'/tmp/services.log');
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":resource:".$resource."\n",3,'/tmp/services.log');
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":details :".$details."\n",3,'/tmp/services.log');

		//Define the $results array
		$results = array();

		if(isset($details)){
			$results['message'] = "Malforemed URL";
		}
		else{
			list($resource,$query) = explode('?',$resource,2);
			//$myparms = explode('&',$query);
			$qp = explode('&',$query);
			foreach($qp as $parm){
				//Dont create pairs for empty $parm.  That causes issues in subsequent PHP classes
				if($parm!=""){
					list($key,$value) = explode('=',$parm);
					$GLOBALS['queryParms'][$key]=$value;
				}
			}
			$count = count($GLOBALS['queryParms']);
			switch($count){
				case 0:
					$results['message'] = "No parameters";
					break;
				case 1:
					$results['message'] = isset($GLOBALS['queryParms']['target']) ? 'none' : 'Missing parameter';
					break;
				default:
				$results['message'] = "Too many parameters";
			}

			//The only VALID information we are expecting to be passed as parameter is 'target'. Anything different is useless
			$target = $GLOBALS['queryParms']['target'];

			error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":message=".$results['message']."\n",3,'/tmp/services.log');
			//If data integrity is ok then proceed.  If not then return 
			if($results['message'] === 'none'){
				//Each $domain has its own authentication procedure.  Launch the right one based on $domain value
				//$localConfig = parse_ini_file('./services'.$unit.'_'.$target.'.ini',true);
				$user     = $data['_SERVER']['HTTP_USER'];
				$pass     = $data['_SERVER']['HTTP_PASS'];
				$tier     = $data['_SERVER']['HTTP_TIER'];

				//THIS IS A "CHEAP" HACK AS WE ARE 100% DEPENDENT ON WHERE login3.php IS DEPLOYED... NOT GOOD BUT, AS USUAL, WE ARE IN A HURRY
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".dirname(__FILE__)."\n",3,'/tmp/services.log');
				//require_once('/usr/local/lsws/DEFAULT/html/amfphp/services/login3.php');
				require_once(dirname(__FILE__).'/../../getLDAPusers.class.php');
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":\n",3,'/tmp/services.log');
				//$login = new login3($data);
				$login = new getLDAPusersClass();
				$parms = array('user'=>$user,'password'=>$pass,'domain'=>$domain,'unit'=>$unit,'resource'=>$resource, 'target'=>$target);
				$bufR = print_r($parms,TRUE);
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":parms:\n".$bufR."\n",3,'/tmp/services.log');
				$results = $login->getLDAPusers($parms);
				$bufR = print_r($results,TRUE);
				//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":RESULTS:\n".$bufR."\n",3,'/tmp/services.log');
				error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":hash:".$results['hash']."\n",3,'/tmp/services.log');
			}
		}
		if(array_key_exists('hash',$results)){
			$result = '{"token":"'.$results['hash'].'","error":"none"}';
		}
		else{
			$result = '{"token":"unknown","error":"'.$results['message'].'"}';
		}
		return $result;
	}
}
?>

