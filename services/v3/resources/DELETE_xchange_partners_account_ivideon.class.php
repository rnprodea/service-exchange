<?php
// error_reporting(-1);
// ini_set('log_errors',0);
// ini_set('error_log','/tmp/services.log');
date_default_timezone_set('America/Chicago');
require_once('./common.php');
class DELETE_xchange_partners_account_ivideonClass extends common{
	function DELETE_xchange_partners_account_ivideonClass () {
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":\n",3,'/tmp/services.log');
        parent::__construct();
	}
	function DELETE_xchange_partners_account_ivideon($data){
        // $buf = print_r($data,true);
		// error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":data:\n".$buf."\n",3,'/tmp/services.log');
		// //Each $domain has its own authentication procedure.  Launch the right one based on $domain value
		$partner  = $data['_POST']['partner'];
		$user     = $data['_SERVER']['HTTP_USER'];
		$hash     = $data['_SERVER']['HTTP_HASH'];
		$tier     = $data['_SERVER']['HTTP_TIER'];
		$domain   = $data['domain'];
		$unit     = $data['unit'];
		$target   = $data['target'];
		$token    = $data['queryParms']['token'];
		$id       = $data['queryParms']['id'];
		$login    = $data['queryParms']['login'];
		$host     = $data['queryParms']['host'];
		#$URL      = $host.'/users?op=FIND';
		#$URL     .= $id !== '' ? 'id='.$id : 'login='.$login;
		$localConfig = parse_ini_file('./services_'.$target.'.ini',true);
		#$host         = $localConfig['accountCreationHost'];
		$URL         = $host.'/users/'.$id.'?op=DELETE';
		$buf = print_r($data['queryParms'],true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":parameters=".$buf."\n",3,'/tmp/services.log');
		//Break URI in its components
		list($null,$void,$version,$domain,$project,$resource,$details) = explode('/',$data['_SERVER']['REQUEST_URI'],7);
		$error = 'none';
		$headers = array(
		    'Content-Type:multipart/form-data',
		    'Authorization:Bearer '.$token
		);
		// #$URL = $localConfig['sessionURL'];
		// if($login == ''){
		// 	$postData = '{"id":"'.$id.'"}';
		// }
		// else{
		// 	$postData = '{"login":"'.$login.'"}';			
		// }
		error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":URL=".$URL."\n",3,'/tmp/services.log');
		// $postData = '{"login":"'.$data['queryParms']['login'].'", "password":"'.$data['queryParms']['password'].'"}';
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_POST,true);
		#curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);
		curl_setopt($curl,CURLOPT_URL,$URL);
		
		#Un-comment these lines for cURL debugging
		#$curl_log = fopen("/tmp/curl.log", 'w');
		#curl_setopt($curl,CURLOPT_VERBOSE, true);
		#curl_setopt($curl,CURLOPT_STDERR,$curl_log);
		#curl_setopt($curl,CURLINFO_HEADER_OUT,true);

		#$buf = print_r($curl,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":URL=".$URL."\n",3,'/tmp/services.log');


		$results = curl_exec($curl);
		$resultsj = json_decode($results,true);
		$status  = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$info    = curl_getinfo($curl);
		$error   = curl_error($curl);
		curl_close($curl);		

		$buf = print_r($resultsj,true);
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$buf."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":results=".$results."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":error=".$error."\n",3,'/tmp/services.log');
        error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":".__FUNCTION__.":status=".$status."\n",3,'/tmp/services.log');

		if($status == 200){
			//error_log(date("Ymd H:i:s", time()) .":". basename(__FILE__).".".__LINE__.":token = ".$token."\n",3,'/tmp/services.log');
			//Create the time reference for future requests
			$hash = md5($user.$domain.$unit.$target.$hash);
			file_put_contents(getcwd().'/../tokens/'.$user.'.loggedSince.'.$hash,$_SERVER['REQUEST_TIME']);
			$result ='{"success":"true", "id":"'.$id.'","login":"'.$login.'"}';

		}
		else{
			$message = "Unknown reason";
			switch ($status) {
			    case 400:
			        $message =  "Wrong API Host";
			        break;
			}
			$result ='{"success":"false", "status":"'.$status.'", "message":"'.$message.'"}';
		}
		return $result;
	}
}
?>

