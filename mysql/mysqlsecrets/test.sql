-- MySQL dump 10.13  Distrib 5.5.61, for linux-glibc2.12 (x86_64)
--
-- Host: localhost    Database: services
-- ------------------------------------------------------
-- Server version	5.5.61

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `services`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `services` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `services`;

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agid` int(11) NOT NULL,
  `sgid` int(11) NOT NULL,
  `wgid` int(11) NOT NULL,
  PRIMARY KEY (`id`,`agid`,`sgid`,`wgid`),
  UNIQUE KEY `access` (`agid`,`sgid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access`
--

LOCK TABLES `access` WRITE;
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
/*!40000 ALTER TABLE `access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accessgroup`
--

DROP TABLE IF EXISTS `accessgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accessgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) NOT NULL,
  PRIMARY KEY (`id`,`name`),
  UNIQUE KEY `accessgroup` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accessgroup`
--

LOCK TABLES `accessgroup` WRITE;
/*!40000 ALTER TABLE `accessgroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `accessgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accesssubscription`
--

DROP TABLE IF EXISTS `accesssubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesssubscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agid` int(11) NOT NULL,
  `ugid` int(11) NOT NULL,
  PRIMARY KEY (`id`,`agid`,`ugid`),
  UNIQUE KEY `subscription` (`agid`,`ugid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesssubscription`
--

LOCK TABLES `accesssubscription` WRITE;
/*!40000 ALTER TABLE `accesssubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `accesssubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enablers`
--

DROP TABLE IF EXISTS `enablers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enablers` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `signature` varchar(24) NOT NULL,
  `type` enum('broker','provider','xchange') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `enabler` (`signature`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enablers`
--

LOCK TABLES `enablers` WRITE;
/*!40000 ALTER TABLE `enablers` DISABLE KEYS */;
INSERT INTO `enablers` VALUES (1,'prodea','prodea','xchange'),(2,'iVideon','ivideon','provider'),(3,'Total Play','totalplay','broker');
/*!40000 ALTER TABLE `enablers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `endpoint`
--

DROP TABLE IF EXISTS `endpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `endpoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `target` varchar(64) NOT NULL,
  `targettype` enum('bid','pid','xid') NOT NULL,
  PRIMARY KEY (`id`,`sid`),
  UNIQUE KEY `endpoint` (`sid`,`target`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `endpoint`
--

LOCK TABLES `endpoint` WRITE;
/*!40000 ALTER TABLE `endpoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `endpoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nodedetails`
--

DROP TABLE IF EXISTS `nodedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodedetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) NOT NULL,
  `type` varchar(24) DEFAULT NULL,
  `class` varchar(40) NOT NULL,
  `instance` varchar(60) NOT NULL,
  PRIMARY KEY (`id`,`nid`),
  UNIQUE KEY `details` (`nid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nodedetails`
--

LOCK TABLES `nodedetails` WRITE;
/*!40000 ALTER TABLE `nodedetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `nodedetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nodes`
--

DROP TABLE IF EXISTS `nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid` smallint(6) NOT NULL,
  `bnodeid` varchar(64) DEFAULT NULL,
  `pid` smallint(6) NOT NULL,
  `pnodeid` varchar(64) DEFAULT NULL,
  `xid` smallint(6) NOT NULL,
  `xnodeid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `node` (`xnodeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nodes`
--

LOCK TABLES `nodes` WRITE;
/*!40000 ALTER TABLE `nodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `verb` enum('GET','PUT','DELETE','POST') NOT NULL,
  `domain` varchar(24) NOT NULL,
  `unit` varchar(24) NOT NULL,
  `resource` varchar(24) NOT NULL,
  PRIMARY KEY (`id`,`verb`,`domain`,`unit`,`resource`),
  UNIQUE KEY `url` (`verb`,`domain`,`unit`,`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicegroup`
--

DROP TABLE IF EXISTS `servicegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`,`name`),
  UNIQUE KEY `servicegroup` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicegroup`
--

LOCK TABLES `servicegroup` WRITE;
/*!40000 ALTER TABLE `servicegroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `servicegroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicesubscription`
--

DROP TABLE IF EXISTS `servicesubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicesubscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sgid` int(11) NOT NULL,
  `epid` int(11) NOT NULL,
  PRIMARY KEY (`id`,`sgid`,`epid`),
  UNIQUE KEY `subscription` (`sgid`,`epid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicesubscription`
--

LOCK TABLES `servicesubscription` WRITE;
/*!40000 ALTER TABLE `servicesubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `servicesubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timewindow`
--

DROP TABLE IF EXISTS `timewindow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timewindow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `type` enum('year','dayofyear','month','dayofmonth','week','dayofweek','hour','minute') NOT NULL,
  `from` smallint(6) NOT NULL,
  `to` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timewindow`
--

LOCK TABLES `timewindow` WRITE;
/*!40000 ALTER TABLE `timewindow` DISABLE KEYS */;
/*!40000 ALTER TABLE `timewindow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timewindowgroup`
--

DROP TABLE IF EXISTS `timewindowgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timewindowgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timewindowgroup`
--

LOCK TABLES `timewindowgroup` WRITE;
/*!40000 ALTER TABLE `timewindowgroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `timewindowgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timewindowsubscription`
--

DROP TABLE IF EXISTS `timewindowsubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timewindowsubscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `twgid` int(11) NOT NULL,
  `twid` int(11) NOT NULL,
  PRIMARY KEY (`id`,`twgid`,`twid`),
  UNIQUE KEY `subscription` (`twgid`,`twid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timewindowsubscription`
--

LOCK TABLES `timewindowsubscription` WRITE;
/*!40000 ALTER TABLE `timewindowsubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `timewindowsubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xchuser`
--

DROP TABLE IF EXISTS `xchuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xchuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `pass` blob,
  PRIMARY KEY (`id`,`name`),
  UNIQUE KEY `user` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xchuser`
--

LOCK TABLES `xchuser` WRITE;
/*!40000 ALTER TABLE `xchuser` DISABLE KEYS */;
INSERT INTO `xchuser` VALUES (1,'ops_rnagles','��2ĽaI�');
/*!40000 ALTER TABLE `xchuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xchusergroup`
--

DROP TABLE IF EXISTS `xchusergroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xchusergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`,`name`),
  UNIQUE KEY `group` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xchusergroup`
--

LOCK TABLES `xchusergroup` WRITE;
/*!40000 ALTER TABLE `xchusergroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `xchusergroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xchusersubscription`
--

DROP TABLE IF EXISTS `xchusersubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xchusersubscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xuid` int(11) NOT NULL,
  `xugid` int(11) NOT NULL,
  PRIMARY KEY (`id`,`xuid`,`xugid`),
  UNIQUE KEY `xuid` (`xuid`,`xugid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xchusersubscription`
--

LOCK TABLES `xchusersubscription` WRITE;
/*!40000 ALTER TABLE `xchusersubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `xchusersubscription` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-04  5:20:02
